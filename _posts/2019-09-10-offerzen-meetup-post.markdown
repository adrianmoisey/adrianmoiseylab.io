---
layout:     post
title:      "6 Pro-Tips To Host a Kick-Ass Meetup"
date:       2019-09-10 09:00:00
author:     "Adrian Moisey"
---

# 6 Pro-Tips To Host a Kick-Ass Meetup

A few months ago I teamed up with OfferZen and wrote an article on what I learnt from running a meetup group, 
Cape Town DevOps Meetup.

This article also includes some tips I learnt along the way.

It's hosted over on [OfferZen's Blog](https://www.offerzen.com/blog/6-pro-tips-to-host-a-kick-ass-meetup)
