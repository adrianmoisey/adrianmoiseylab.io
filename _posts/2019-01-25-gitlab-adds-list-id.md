---
layout:     post
title:      "Filtering emails from GitLab in Gmail"
date:       2019-01-25 17:00:00
author:     "Adrian Moisey"
---

# Filtering emails from GitLab in Gmail

I use GitLab for a few different groups. My employer stores their code there, I'm involved with random open source
projects and the [Cape Town DevOps Meetup Group](https://www.meetup.com/Cape-Town-DevOps/) uses GitLab to organise meetups.

Up until recently, I've found that I struggled to filter my incoming emails correctly. Between GitLab and Gmail, I had no
way to filter out each organisation into different labels. Sometimes this meant that certain issues I missed some
emails, particularly from issues that weren't very busy.

I'm happy to say that a [recent community contribution](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/22817) to
GitLab has solved this issue for me! GitLab 11.7 adds the [List-Id](https://tools.ietf.org/html/rfc2919) header to
emails sent to you, allowing you to filter on group or project.

In Gmail, you can simply search for `list:<organisation>` (eg. `list:list:gitlab-org`), and create your filter from that.
My preferred configuration is to create a label per organisation that I follow and filter all those emails accordingly.
