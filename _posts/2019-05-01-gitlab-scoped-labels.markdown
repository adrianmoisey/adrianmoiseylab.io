---
layout:     post
title:      "GitLab adds Scoped Labels"
date:       2019-05-01 11:00:00
author:     "Adrian Moisey"
---

# GitLab adds Scoped Labels

[Scoped labels](https://about.gitlab.com/2019/04/22/gitlab-11-10-released/#scoped-labels) were introduced in [GitLab 11.10](https://about.gitlab.com/2019/04/22/gitlab-11-10-released/).

These can only be used in GitLab Premium/Silver (and Ultimate/Gold), which is unfortunate because none of my private
projects are on that level.

Scoped labels are mutually exclusive labels that share a namespace (or scope). These are created with the format of
`<scope>::<name>` (note the double `::`).

Two scoped labels with the same scope but a different value cannot be simultaneously added apply to an issue.

## Example use case

You might want to have labels that indicate the priority of an issue. 
You can create multiple labels with the same scope: `priority::high`, `priority::medium` and `priority::low`
This would mean that if you have an issue with a `medium` priority and changed it to `high`, GitLab will
automatically remove the `medium` label for you. Handy!

GitLab's documentation also has an example for using these labels with their Issue Board in the
[documentation](https://docs.gitlab.com/ee/user/project/labels.html#workflows-with-scoped-labels-premium)

## Improvement I'd like to see

I can't currently find a way to search scoped labels by their scope. 
For example, I may want to search for all labels beginning in `priority::`.
I hope GitLab adds functionality around that.
